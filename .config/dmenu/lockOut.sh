#!/usr/bin/bash

#Choose editor
editor="emacs --no-splash"

#Choose list entries for dmenu
list=(Ausschalten Neustarten Abmelden Abbrechen)

#Show Menu
choice=$(printf '%s\n' ${list[@]} | dmenu -c -bw 0 -l 20 -fn "mononoki nerd font" -p "Auswahl:")

#Execute choice
if [ "$choice" == ${list[0]} ]
then
    echo "${list[0]}"
    shutdown -h now
elif [ "$choice" == ${list[1]} ]
then
    echo "${list[1]}"
    shutdown -r now
elif [ "$choice" == ${list[2]} ]
then
    echo "${list[2]}"
    dm-tool switch-to-greeter
elif [ "$choice" == ${list[3]} ]
then
    echo "${list[3]}"
fi
