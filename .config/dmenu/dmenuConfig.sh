#!/usr/bin/bash
editor="emacsclient -c -a emacs"
path="$HOME/.config/dmenu"
choice=$(ls -1F $path | grep -v / | dmenu -bw 0 -l 20 -fn "mononoki nerd font")
if [ "$choice" ]
then
    $editor $path/${choice}
fi
