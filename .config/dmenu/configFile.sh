#!/usr/bin/bash -x

#dmenu='dmenu -bw 0 -l 20 -fn "mononoki nerd font"'
editor="emacsclient -c -a emacs"

pathConfig="$HOME/.config"

list=(qtile termite alacritty picom)
configFile=("config.py" "config" "alacritty.yml" "picom.conf")

choice=$(printf '%s\n' ${list[@]} | dmenu -bw 0 -l 20 -fn "mononoki nerd font")

if [ "$choice" ]
then
    
    for i in ${list[*]}
    do
	if [ $i == $choice ]
	then
	    break
	fi
	index=$((index+1))
    done
   
    $editor $HOME/.config/${choice}/${configFile[index]}
fi
