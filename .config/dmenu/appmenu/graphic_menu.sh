#!/usr/bin/bash
#/usr/bin/bash -x start a debug mode

directory="$HOME/.config/dmenu/appmenu"

menu(){
    printf "Zurück\n"
    printf "Gimp\n"
    printf "Flameshot\n"
    }

choice=$(menu | dmenu -bw 0 -l 20 -fn "mononoki nerd font")

case $choice in
    "Gimp")
	gimp &
	;;
    "Flameshot")
	flameshot &
	;;
    "Zurück")
	$directory/main_menu.sh &
	;;
    *)
	;;
esac
