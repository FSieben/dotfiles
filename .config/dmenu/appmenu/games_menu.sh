#!/usr/bin/bash
#/usr/bin/bash -x start a debug mode

directory="$HOME/.config/dmenu/appmenu"

menu(){
    printf "Zurück\n"
    printf "Steam\n"
    printf "Battle.net\n"
    printf "Hedgewars\n"
    printf "Jagged alliance 2\n"
    }

choice=$(menu | dmenu -bw 0 -l 20 -fn "mononoki nerd font")

case $choice in
    "Steam")
	steam &
	;;
    "Battle.net")
	battlenet.sh &
	;;
    "Hedgewars")
	hedgewars &
	;;
    "Jagged alliance 2")
	ja2.sh &
	;;
    "Zurück")
	$directory/main_menu.sh &
	;;
    *)
	;;
esac
