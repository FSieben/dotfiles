#!/usr/bin/bash
#/usr/bin/bash -x start a debug mode

directory="$HOME/.config/dmenu/appmenu"

menu(){
    printf "Zurück\n"
    printf "Asciiquarium\n"
    printf "Cmatrix\n"
    }

choice=$(menu | dmenu -bw 0 -l 20 -fn "mononoki nerd font")

case $choice in
    "Asciiquarium")
	alacritty -e asciiquarium &
	;;
    "Cmatrix")
	alacritty -e cmatrix &
	;;
    "Zurück")
	$directory/main_menu.sh &
	;;
    *)
	;;
esac
