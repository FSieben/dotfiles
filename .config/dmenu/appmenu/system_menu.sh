#!/usr/bin/bash
#/usr/bin/bash -x start a debug mode

directory="$HOME/.config/dmenu/appmenu"

menu(){
    printf "Zurück\n"
    printf "Terminal\n"
    printf "File manager\n"
    printf "Nvidia-Settings\n"
    printf "Arandr (Monitor arrange)\n"
    printf "Sound Volume\n"
    printf "Themes\n"
    }

choice=$(menu | dmenu -bw 0 -l 20 -fn "mononoki nerd font")

case $choice in
    "Terminal")
	alacritty &
	;;
    "File manager")
	thunar &
	;;
    "Nvidia-Settings")
	nvidia-settings &
	;;
    "Arandr (Monitor arrange)")
	arandr &
	;;
    "Sound Volume")
	pavucontrol &
	;;
    "Themes")
	lxappearance &
	;;
    "Zurück")
	$directory/main_menu.sh &
	;;
    *)
	;;
esac
