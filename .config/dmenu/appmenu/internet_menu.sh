#!/usr/bin/bash
#/usr/bin/bash -x start a debug mode

directory="$HOME/.config/dmenu/appmenu"

menu(){
    printf "Zurück\n"
    printf "Firefox\n"
    printf "Brave\n"
    printf "Chromium\n"
    printf "Thunderbird\n"
    }

choice=$(menu | dmenu -bw 0 -l 20 -fn "mononoki nerd font")

case $choice in
    "Firefox")
	firefox &
	;;
    "Brave")
	brave &
	;;
    "Chromium")
	chromium &
	;;
    "Thunderbird")
	thunderbird &
	;;
    "Zurück")
	$directory/main_menu.sh &
	;;
    *)
	;;
esac
