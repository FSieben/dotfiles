# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, qtile, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen, KeyChord
from libqtile.lazy import lazy
import subprocess
from subprocess import PIPE
#import getVolume
import os

mod = "mod4"
terminal = "alacritty"
homeDirectory = os.path.expanduser('~')
EDITOR_EMACS = "emacsclient -c -a emacs"
NET_DEVICE = "wlan0"
BATTERY = True

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    Key([mod, "control"], "n", lazy.next_screen(), desc="Keyboard focus to monitor 1"),
    Key([mod, "control"], "p", lazy.prev_screen(), desc="Keyboard focus to monitor 2"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key(["mod1", "control"], "l", lazy.spawn("light-locker-command -l"), desc="Lock Session"),
    Key([], "Print", lazy.spawn("flameshot"), desc="Start screenshot tool"),
    Key([mod], "Print", lazy.spawn("flameshot full -c"), desc="Start screenshot tool"),
    Key([], "XF86PowerOff", lazy.spawn(homeDirectory + "/.config/dmenu/lockOut.sh"), desc="Power On/Off menu"),
    Key([], "XF86Sleep", lazy.spawn(homeDirectory + "/.config/dmenu/lockOut.sh"), desc="Power On/Off menu"),
    #Key([mod], "r", lazy.spawncmd(),
        #desc="Spawn a command using a prompt widget"),
    Key([mod], "r", lazy.spawn("dmenu_run -bw 0 -fn \"mononoki nerd font\" -p \"Run:\""),
        desc="Spawn a command using a prompt widget"),
    #Key([mod], "f", lazy.hide_show_bar("top"), desc="Show hide bar (top)"),
    Key([mod], "f", lazy.window.toggle_fullscreen()),

    # Volume control
#    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-mute 0 false; pactl set-sink-volume 0 +5%"), desc = "Increase Volume"),
#    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-mute 0 false; pactl set-sink-volume 0 -5%"), desc = "Decrease Volume"),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%"), desc = "Increase Volume"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%"), desc = "Decrease Volume"),
    Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")),

    # Function key mapping
    Key([mod], "b", lazy.spawn("firefox"), desc = "Browser"),
    Key([mod, "shift"], "p", lazy.spawn("passmenu"), desc="passmenu to get a password"),
    Key([mod], "m", lazy.spawn(homeDirectory + "/.config/dmenu/appmenu/main_menu.sh"), desc = "Demnu app menu"),
    Key([], "XF86HomePage", lazy.spawn("firefox"), desc = "Browser"),
    Key([], "XF86Mail", lazy.spawn("thunderbird"), desc = "Mailclient"),
    Key([], "XF86Calculator", lazy.spawn("galculator"), desc = "Calculator"),
    
    # Emacs programs launched using the key chord CTRL+e followed by 'key'
    KeyChord([mod],"e", [
        Key([], "e",
            lazy.spawn(EDITOR_EMACS + " --eval '(dashboard-refresh-buffer)'"),
            #lazy.spawn(EDITOR_EMACS),
            desc='Launch Emacs'
            ),
        Key([], "c",
            lazy.spawn(homeDirectory + "/.config/dmenu/configFile.sh"),
            desc='Launch dmenu edit config'
            ),
        Key([], "d",
            lazy.spawn(homeDirectory + "/.config/dmenu/dmenuConfig.sh"),
            desc='Launch dmenu edit dmenu config file'
            )
        ]),

    # Set focus to screen x
    KeyChord([mod],"m", [
        Key([], "1",
            lazy.to_screen(0),
            desc="Keyboard focus to monitor 1"
            ),
        Key([], "2",
            lazy.to_screen(1),
            desc="Keyboard focus to monitor 2"
            )
        ]),

]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(toggle=True),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
#        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
#            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
         Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
             desc="move focused window to group {}".format(i.name)),
    ])

layouts = [
    layout.Tile(border_focus="ff0000", border_width=1, margin = 0),
    layout.Columns(border_focus="ff0000", border_focus_stack='#d75f5f', border_width=1, margin = 0),
    layout.Max(),
    layout.Floating(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2,
    #             margin = 3),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='mononoki Nerd Font',
    fontsize=18,
    padding=10,
    foreground="#a3a3c2",
)
extension_defaults = widget_defaults.copy()

widget_TextBox_ArchSign={"fmt":"\uF303",
                         "padding":None,
                         "foreground":"#1793D1",
                         "mouse_callbacks":{"Button1":lambda: qtile.cmd_spawn(homeDirectory + "/.config/dmenu/appmenu/main_menu.sh")}}
widget_Sep={"foreground":"#9900ff", "padding":10}
widget_GroupBox={"padding":1,
                 "borderwidth":2,
                 "this_current_screen_border":"#5c0099",
                 "this_screen_boarder":"#5c0099",
                 "active":"#a3a3c2"}
widget_Systray={"icon_size":16}
widget_ThermalSensor={"fmt":"\uE20A {:>.2}°C",
                      "tag_sensor":"Package id 0",
                      "foreground":"#a3a3c2"}
widget_CPU={"format":"\uE266 {load_percent:>5}%"}
widget_Net={"interface":NET_DEVICE,
            "format":"\uE619 {down}/{up}"}
widget_PulsVolume={"fmt":"\uF028 {:>4}",
                   "mouse_callbacks":{"Button1": lambda: qtile.cmd_spawn("pavucontrol")}}
widget_Clock={"format":"\uF017 %a %d.%m.%Y-%H:%M",
              "mouse_callbacks":{"Button1": lambda: qtile.cmd_spawn(terminal + "cal")}}
widget_Layout={"custom_icon_paths":[homeDirectory + "/.config/qtile/icons/layout"]}
widget_TextBox_Shutdown={"fmt":"\uF011",
                         "mouse_callbacks":{"Button1": lambda: qtile.cmd_spawn(homeDirectory + "/.config/dmenu/lockOut.sh")}}
widget_Battery={"fmt":"\uf240  {}",
                "update_interval":5,
                "charge_char":'\u2191',
                "discharge_char":'\u2193'}

if not BATTERY:
    main_bar = [
                widget.TextBox(**widget_TextBox_ArchSign),
                widget.Sep(**widget_Sep),
                widget.GroupBox(**widget_GroupBox),
                widget.Sep(**widget_Sep),
                widget.Systray(**widget_Systray),
                widget.Sep(**widget_Sep),
                widget.WindowName(),
                widget.Sep(**widget_Sep),
                widget.ThermalSensor(**widget_ThermalSensor),
                widget.Sep(**widget_Sep),
                widget.CPU(**widget_CPU),
                widget.Sep(**widget_Sep),
                widget.Net(**widget_Net),
                widget.Sep(**widget_Sep),
                widget.PulseVolume(**widget_PulsVolume),
                widget.Sep(**widget_Sep),
                widget.Clock(**widget_Clock),
                widget.Sep(**widget_Sep),
                widget.CurrentLayoutIcon(**widget_Layout),
                widget.Sep(**widget_Sep),
                widget.TextBox(**widget_TextBox_Shutdown),
                ]
else:
    main_bar = [
                widget.TextBox(**widget_TextBox_ArchSign),
                widget.Sep(**widget_Sep),
                widget.GroupBox(**widget_GroupBox),
                widget.Sep(**widget_Sep),
                widget.Systray(**widget_Systray),
                widget.Sep(**widget_Sep),
                widget.WindowName(),
                widget.Sep(**widget_Sep),
                widget.Battery(**widget_Battery),
                #widget.ThermalSensor(**widget_ThermalSensor),
                #widget.Sep(**widget_Sep),
                #widget.CPU(**widget_CPU),
                widget.Sep(**widget_Sep),
                widget.Net(**widget_Net),
                widget.Sep(**widget_Sep),
                widget.PulseVolume(**widget_PulsVolume),
                widget.Sep(**widget_Sep),
                widget.Clock(**widget_Clock),
                widget.Sep(**widget_Sep),
                widget.CurrentLayoutIcon(**widget_Layout),
                widget.Sep(**widget_Sep),
                widget.TextBox(**widget_TextBox_Shutdown),
                ]
screens = [
    Screen(
        top=bar.Bar(
            # [
            #  widget.TextBox(**widget_TextBox_ArchSign),
            #  widget.Sep(**widget_Sep),
            #  widget.GroupBox(**widget_GroupBox),
            #  widget.Sep(**widget_Sep),
            #  widget.WindowName(), widget.Systray(**widget_Systray),
            #  widget.Sep(**widget_Sep),
            #  widget.ThermalSensor(**widget_ThermalSensor),
            #  widget.Sep(**widget_Sep),
            #  widget.CPU(**widget_CPU),
            #  widget.Sep(**widget_Sep),
            #  widget.Net(**widget_Net),
            #  widget.Sep(**widget_Sep),
            #  widget.PulseVolume(**widget_PulsVolume),
            #  widget.Sep(**widget_Sep),
            #  widget.Clock(**widget_Clock),
            #  widget.Sep(**widget_Sep),
            #  widget.CurrentLayoutIcon(**widget_Layout),
            #  widget.Sep(**widget_Sep),
            #  widget.TextBox(**widget_TextBox_Shutdown),
            # ],
            main_bar,
            20,
        ),
    ),
    Screen(
        top=bar.Bar(
            [
            widget.GroupBox(**widget_GroupBox),
            widget.WindowName(),
            widget.Clock(**widget_Clock),
            widget.Sep(**widget_Sep),
            widget.CurrentLayoutIcon(**widget_Layout),
            ],
            20,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(title='Presentation On or Off'),
    Match(wm_class='tk'),
    Match(wm_class='galculator'),
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

@hook.subscribe.startup_once
def autostart():
    qtile.cmd_spawn(homeDirectory + "/.config/qtile/autostart.sh")
