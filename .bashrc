#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls -hlF --color=auto'
alias grep='grep --color'
alias coding='cd ~/Programmieren/git'
alias config='/usr/bin/git --git-dir=$HOME/dotfiles.git/ --work-tree=$HOME'
alias update='sudo pacman -Syu'

#Uncommend for colored prompt
color_prompt=yes

if [ "$color_prompt" = yes ]; then
    #        PS1='\[\e[38;5;2m\]\u\[\e[38;5;158m\]@\[\e[38;5;2m\]\h\[\e[00m\]:\[\e[01;34m\]\w\[\e[00m\]\$ '
    PS1='\[\e[38;5;2m\][\u]\[\e[01;34m\][\w]\[\e[00m\]:\$>'
else
    PS1='\u@\h:\w\$ '
fi

# Add $HOME/bin to $PATH
export PATH="$HOME/bin:$PATH"
